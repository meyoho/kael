# from django.conf import settings
from utils.http import JsonResponse
import utils.sms
import json
import logging
# import datetime


logger = logging.getLogger(__name__)


def send_sms(request):
    """ Sending a short message
    """
    logger.debug("Received sms sending request")
    data = json.loads(request.body)
    to = data['to']
    subject = data['subject']
    msg = data['message']
    r = utils.sms.send_sms(",".join(to), subject, msg)
    data['success'] = r
    logger.debug("Sending sms to {} over {}. {}".format(to, r, subject))
    return JsonResponse(data)
