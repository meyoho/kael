from tools import views
from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',  # the '' is necessary,avoid /endpoints/ Page Not Found Error.
    # url(r'^$', views.tools),
    url(r'^_ping', views.ping),
    url(r'^push_to_dashboard', views.push_to_dashboard),
)
