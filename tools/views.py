# -*- coding: utf-8 -*-

from django.conf import settings
# from django.http import HttpResponse
from rest_framework.decorators import api_view
from utils.http import JsonResponse
import sys
import json
import logging
# import datetime
from prometheus_client import Gauge, CollectorRegistry, pushadd_to_gateway

reload(sys)
sys.setdefaultencoding('utf-8')

logger = logging.getLogger(__name__)


def return_success(data=None):
    if data is None:
        return {"result": "OK"}
    else:
        return {"result": "OK", "data": data}


def return_error(code, msg):
    return {"result": "ERROR", "code": code, "msg": msg}


@api_view(['GET'])
def ping(request):
    return JsonResponse(return_success())


def push_metrics(metrics):
    for data in metrics:
        label_list = data["labels"]
        reg = CollectorRegistry(auto_describe=False)
        g = Gauge(data["metric_name"], data["description"],
                  label_list, registry=reg)
        # g.set_to_current_time()

        for item in data["samples"]:
            t = []
            for l in label_list:
                if "labels" in item:
                    t = t + [item["labels"][l]]
                else:
                    t = t + [item[l]]
            v = item["value"]
            g.labels(*t).set(v)
            # print(t, v)

        pushadd_to_gateway(settings.PUSHGATEWAY_URL,
                           job=data["job"],
                           registry=reg,
                           timeout=settings.PUSHGATEWAY_TIMEOUT)


@api_view(['POST'])
def push_to_dashboard(request):
    logger.debug("push_to_dashboard(): {}".format(request.body))
    try:
        metrics = json.loads(request.body)
    except Exception as e:
        return JsonResponse(return_error("ERROR_POST_DATA_FORMAT", str(e)))

    try:
        push_metrics(metrics)
        return JsonResponse(return_success(metrics))
    except Exception as e:
        return JsonResponse(return_error("ERROR_PUSH_DATA", str(e)))
