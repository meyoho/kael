# from django.conf import settings
from utils.http import JsonResponse
import utils.dingtalk
import json
import logging
# import datetime


logger = logging.getLogger(__name__)


def send_dingtalk(request):
    """ Sending dingtalk message
    """
    logger.debug("Received dingtalk sending request")
    data = json.loads(request.body)
    to = ",".join(data['to'])
    msg = data['message']
    r = utils.dingtalk.send_dingtalk(to, msg)
    data['success'] = r
    logger.debug("Sending dingtalk to {}. {}".format(to, r))
    return JsonResponse(data)
