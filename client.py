import requests
import json


class KaelClient():
    def __init__(self, host, port):
        self.url = "http://{}:{}/v1/".format(host, port)

    def __get_service_result(self, url, payload):
        header = {'content-type': 'application/json'}
        data = json.dumps(payload)
        r = requests.post(url, data=data, headers=header)
        if r.status_code >= 300:
            return False

        obj = json.loads(r.text)
        return obj["success"]

    def send_email(self, tolist, subject, body, mtype='plain'):
        url = self.url + "mail/"
        payload = {
            "type": mtype,
            "to": tolist,
            "subject": subject,
            "body": body
        }
        return self.__get_service_result(url, payload)

    def send_sms(self, tolist, subject, message):
        url = self.url + "sms/"
        payload = {
            "to": tolist,
            "subject": subject,
            "message": message
        }
        return self.__get_service_result(url, payload)

    def phone_call(self, tolist):
        url = self.url + "call/"
        payload = {
            "to": tolist,
            "from": "18610195161"
        }
        return self.__get_service_result(url, payload)


if __name__ == "__main__":
    c = KaelClient("52.11.67.133", 8080)
    # print c.send_email(["ruicao@alauda.io"], "title", "email txt")
    # print c.send_sms(["17778058864"], "title", "sms content")
    # print c.phone_call(["18610195161"])
