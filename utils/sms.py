import utils.yuntongxun
import datetime


def send_sms(to, subject, msg):
    nowtime = datetime.datetime.now() + datetime.timedelta(hours=8)
    nowtime_str = nowtime.strftime('%Y-%m-%d %H:%M:%S')
    return utils.yuntongxun.send_sms(to, subject, msg, nowtime_str)
