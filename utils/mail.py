from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
# from time import sleep, time
import smtplib
# import requests
# import json
import os


# SMTP_FROMS: info@alauda.io
# SMTP_HOST: email-smtp.us-east-1.amazonaws.com
# SMTP_PORT: 465
# SMTP_USERNAME: AKIAISIQSB4UQ42ELJJA
# SMTP_PASSWORD: AkXKD8aO4qEX4xqet+UooAU6LTuRC2iGxyE3JCNWePMz

# SMTP_FROMS: info@alauda.io
# SMTP_HOST: smtp.mailgun.org
# SMTP_PORT: 465
# SMTP_USERNAME: postmaster@sandbox9fe5d7261040492baf1e4e9313bb31b5.mailgun.org
# SMTP_PASSWORD: 194caaf3af3284aa531b247797db356a

# SMTP_FROMS: alauda01@sina.com
# SMTP_HOST: smtp.sina.com
# SMTP_PORT: 465
# SMTP_USERNAME: alauda01
# SMTP_PASSWORD: Mathilde1861

SMTP = {
    'froms': os.getenv('SMTP_FROMS', 'alauda01@sina.com'),
    'host': os.getenv('SMTP_HOST', 'smtp.sina.com'),
    'port': os.getenv('SMTP_PORT', 465),
    'username': os.getenv('SMTP_USERNAME', 'alauda01'),
    'password': os.getenv('SMTP_PASSWORD', 'Mathilde1861'),
    'debug_level': 'DEBUG'
}


def send_email(subject, body, recipients, mtype, file_name=None, file_name_as=None):
    """class method to send an email"""

    # if settings.EMAIL is None or settings.SMTP is None:
    #    logger.error("No email/smtp config, email not sent.")
    #    return

    if not isinstance(recipients, list):
        print "{} should be a list".format(recipients)
        return

    # build message
    msg = MIMEMultipart()
    msg['From'] = SMTP['froms']
    msg['To'] = ','.join(recipients)
    msg['Subject'] = Header(subject, 'utf8')
    msg.attach(MIMEText(body, mtype, 'utf8'))

    if file_name:
        _file = MIMEText(open('%s' % file_name, 'rb').read(),
                         'base64', 'utf-8')
        _file["Content-Type"] = 'application/octet-stream'
        _file["Content-Disposition"] = 'attachment; filename="%s"' % file_name_as
        msg.attach(_file)

    server = None
    try:
        server = smtplib.SMTP_SSL(host=SMTP['host'], port=SMTP['port'])
        server.set_debuglevel(SMTP['debug_level'])
        server.login(SMTP['username'], SMTP['password'])
        server.sendmail(SMTP['froms'], recipients, msg.as_string())
        retval = True
    except Exception as e:
        # don't fatal if email was not send
        retval = False
    finally:
        if server:
            server.quit()
    return retval

if __name__ == "__main__":
    rcvs = ["ruicao@alauda.io"]
    send_email("test".encode('utf-8'), "body", rcvs,
               'html', '/app/utils/models.py', 'xxx.txt')
