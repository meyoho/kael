import requests
import json
import datetime


def send_dingtalk(to, msg):
    nowtime = datetime.datetime.now() + datetime.timedelta(hours=8)
    nowtime_str = nowtime.strftime('%Y-%m-%d %H:%M:%S')

    token = "5c6b8384cd7b0737836f1fdae0fee5a71ef152c83a835dc61baa16fb0a406f94"
    url = "https://oapi.dingtalk.com/robot/send?access_token=" + token
    headers = {
        "content-type": "application/json",
        # "Postman-Token": "4ef86729-0d89-154c-ddd7-770d84e7e7b1",
        "Cache-Control": "no-cache"
    }
    payload = {"to": to, "message": nowtime_str + " " + msg}
    payload = {"msgtype": "text", "text": {"content": msg + " " + nowtime_str}}
    try:
        r = requests.post(url, data=json.dumps(payload), headers=headers)
        print (r.status_code, r.text)
        if r.status_code != 200:
            return False

        # {"errcode":0,"errmsg":"ok"} or
        # {"errcode":300001,"errmsg":"token is not exist"}
        text = r.text
        obj = json.loads(text)
        if obj["errcode"] == 0:
            return True
        return False
    except Exception as e:
        print "Exception: {}".format(e)
        return False

if __name__ == "__main__":
    send_dingtalk("test", "test2test")
