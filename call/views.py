# from django.conf import settings
from utils.http import JsonResponse
import utils.call
import json
import logging
# import datetime


logger = logging.getLogger(__name__)


def send_phonecall(request):
    """ Sending a phone call
    """
    logger.debug("Received call sending request")
    data = json.loads(request.body)
    to = data['to']
    if 'from' not in data:
        data['from'] = '18610195161'
    froms = data['from']
    r = utils.call.call(to, froms)
    data['success'] = r
    logger.debug("Sending call from {} to {} over {}.".format(froms,
                                                              json.dumps(to),
                                                              r))
    return JsonResponse(data)
