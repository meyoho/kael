# from django.conf import settings
# from django.http import HttpResponse
from utils.http import JsonResponse
from utils.mail import send_email
import json
import logging
# import datetime


logger = logging.getLogger(__name__)


def send_mail(request):
    """ Sending email
    """
    logger.debug("Received email sending request")
    data = json.loads(request.body)
    if 'type' not in data:
        data['type'] = 'plain'
    if 'body' not in data:
        data['body'] = ''
    if 'subject' not in data:
        data['subject'] = 'No Title'
    if 'to' not in data:
        data['success'] = False
        data['errmsg'] = "Error in sending email! No recipients."
        logger.debug(data['errmsg'])
        return JsonResponse(data)

    mtype = data['type']
    body = data['body']
    subject = data['subject']
    to = data['to']
    r = send_email(subject, body, to, mtype)
    data['success'] = r
    logger.debug("Sending email to {} over {}. {}".format(json.dumps(to),
                                                          r, subject))
    return JsonResponse(data)
