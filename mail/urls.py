from mail import views
from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',  # the '' is necessary,avoid /endpoints/ Page Not Found Error.
    url(r'^$', views.send_mail),
)
