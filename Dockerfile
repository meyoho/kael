FROM ubuntu:trusty
MAINTAINER 'Rui Cao' ruicao@alauda.io

RUN apt-get update && \
    apt-get install -y libpq-dev python-pip python-dev nginx curl vim && \
    pip install uwsgi flask prometheus_client tencentcloud-sdk-python && \
    mkdir -p /var/log/mathilde/ && \
    chmod 775 /var/log/mathilde/ && \
    mkdir /app

RUN easy_install supervisor
RUN easy_install supervisor-stdout 

# nginx config
RUN rm -rf /etc/nginx/sites-enabled/default && \
    echo "daemon off;" >> /etc/nginx/nginx.conf

EXPOSE 1806

CMD ["/app/run.sh"]

WORKDIR /app


COPY requirements.txt /app
RUN pip install -r /app/requirements.txt

COPY . /app
RUN ln -s /app/conf/nginx.conf /etc/nginx/sites-enabled/nginx.conf && \
    ln -s /app/conf/supervisord.conf /etc/supervisord.conf && \
    chmod +x /app/run.sh
